module  sample {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    requires littleproxy;
    requires netty.all;
    requires littleproxy.mitm;
    requires dec;
    requires org.apache.commons.io;
    requires guava;
    requires org.jetbrains.annotations;
    requires org.apache.commons.compress;
    requires org.apache.commons.lang3;
    requires gson;
    requires java.rmi;

    opens sample;
}