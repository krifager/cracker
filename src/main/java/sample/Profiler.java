package sample;

import javafx.application.Platform;
import javafx.scene.chart.XYChart;

import java.util.ArrayList;
import java.util.List;

/***********************************************************************************************************************
 **************************CREATES A PROFILE FOR A USER WITH INFO ON CATEGORY OF WEBSITES VISITED***********************
 **********************************************************************************************************************/

public class Profiler {
    private final int NUMBER_OF_CATEGORIES = 5;
    private List<String> categories;
    private volatile List<Integer> categoryCount;

    public Profiler(){
        categories = new ArrayList<>();                 //INITIALIZE CATEGORY ARRAYLIST
        categoryCount = new ArrayList<>();              //INITIALIZE CATEGORYCOUNT ARRAYLIST

        for( int i = 0; i < NUMBER_OF_CATEGORIES; i++ ){
            categoryCount.add(0);                       //ADDS NUMBER OF CATEGORIES
        }
        categories.add("news");categories.add("sports");categories.add("game");categories.add("forum");
        categories.add("media");                        //ADDS DATA TO CATEGORIES OF CATEGORY NAMES
    }

    public void updateCategories(String categoryName){
        int count;
        switch(categoryName){               //UPDATES COUNT ON GIVEN CATEGORY FROM GETKEYWORD
            case "news": count = categoryCount.get(0); categoryCount.set(0, ++count); break;
            case "sports": count = categoryCount.get(1); categoryCount.set(1, ++count); break;
            case "game": count = categoryCount.get(2); categoryCount.set(2, ++count); break;
            case "forum": count = categoryCount.get(3); categoryCount.set(3, ++count); break;
            case "media": count = categoryCount.get(4); categoryCount.set(4, ++count); break;
        }

        //UPDATES TO JAVAFX HAS TO RUN ON JAVAFX THREAD
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                //UPDATES BARCHART DATA EVERY TIME UPDATECATEGORY IS CALLED
                XYChart.Series<String, Double> aSeries = new XYChart.Series<String, Double>();

                //CREATES XYCHART DATA OBJECT AND INITIALIZES THE X AND Y VALUES
                aSeries.getData().add(new XYChart.Data("News", categoryCount.get(0)));
                aSeries.getData().add(new XYChart.Data("Sports", categoryCount.get(1)));
                aSeries.getData().add(new XYChart.Data("Game", categoryCount.get(2)));
                aSeries.getData().add(new XYChart.Data("Forum", categoryCount.get(3)));
                aSeries.getData().add(new XYChart.Data("Media", categoryCount.get(4)));

                Controller.list.remove(0);      //REMOVES PREVIOUSLY CREATED SERIES WITH OLD DATA
                Controller.list.add(aSeries);         //UPDATES WITH NEWLY CREATED SERIES WITH NEW DATA
            }
        });
    }

    public List<Integer> getCategoryCount(){
        return categoryCount;
    }
}
