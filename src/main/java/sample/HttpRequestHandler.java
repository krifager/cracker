package sample;

import javafx.concurrent.Task;

/***********************************************************************************************************************
****************************TAKES CARE OF ALL HTTP REQUESTS AND PUTS THEM ON THE GUI************************************
 **********************************************************************************************************************/

public class HttpRequestHandler implements Runnable {
    private String headers;
    private String body;
    private String host;

    //
    public HttpRequestHandler(String headers, String body, String host){
        this.headers = headers;
        this.body = body;
        this.host = host;
    }

    @Override
    public void run() {
        int index = Main.connects.indexOf(this.host);
        //check if host exists
        if(index >= 0){
            Site site = Main.sites.get(index);
            site.addRequest(this.headers, this.body); //STORING SITE INFORMATION IN SITE CLASS
        }
        //System.out.println(this.headers);
    }
}
