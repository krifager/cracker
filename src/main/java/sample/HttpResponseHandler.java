package sample;

import io.netty.handler.codec.http.HttpObject;

import java.util.Scanner;

/***********************************************************************************************************************
 ****************************TAKES CARE OF ALL HTTP REQUESTS AND PUTS THEM ON THE GUI***********************************
 **********************************************************************************************************************/

public class HttpResponseHandler implements Runnable {

    private String headers;
    private String body;
    private String host;
    private String encoding;
    private HttpObject httpObject;

    //CONSTRUCTOR FOR HTTPRESPONSEHANDLER, SETS VALUES FOR HTTPREPONSEHANDLER
    public HttpResponseHandler(HttpObject httpObject, String headers, String body, String encoding) {
        this.headers = headers;
        this.body = body;
        this.encoding = encoding;
        this.host = "";
        this.httpObject = httpObject;
    }

    @Override
    public void run() {
        //IF HTML IS ENCODED, DECODE IT  --> bug: if several encodings used
        boolean hit = false; //WHILE NO HIT
        int index = 0;

        //IF BODY STRING IS NOT EMPTY
        if (!this.body.isEmpty()) {
            /*if(this.encoding != null && !this.encoding.isEmpty()){
                try {
                    HtmlDecoder htmlDecoder = null;
                    htmlDecoder = new HtmlDecoder(this.httpObject, this.body, this.encoding);
                    this.body = htmlDecoder.getDecodedBody();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/

            //GOES THROUGH EVERY SITE AND TRIES TRIES TO CONNECT THE RESPONSE TO A HOST
            while (index < Main.connects.size()) {
                String s = Main.connects.get(index);
                hit = searchForHost(s);
                if (hit) {                      //IF IT FOUND AN EXISTING HOST IN CONNECTS
                    this.host = s;
                    break;
                }
                ++index;
            }
            index = Main.connects.indexOf(this.host);   //SETS INDEX TO HOST, IF THE HOST DOESN'T EXIST SETS INDEX = -1


            //PUT THE HEADERS AND RESPONSES INTO THE RIGHT SITE INSTANCE
            if (hit && index >= 0) {
                String catName;
                Site site = Main.sites.get(index);              //GETS THE SITE FROM SITES LIST IN MAIN
                site.addResponse(this.headers, this.body);      //AND ADDS A RESPONSE ITEM TO THE SITE

                // IF THE HOSTNAME IS IN THE POPULAR SITES LIST, AUTOMATICALLY ASSIGNS CATEGORY
                if(Main.popularSites.contains(this.host)) {
                    switch(this.host){
                        case "facebook": case "twitter": case "instagram": catName = "media"; break;
                        case "youtube": case "twitch": case "mafiaspillet": catName = "game"; break;
                        case "vg": catName = "news"; break;
                        case "stackoverflow": case "blackboard": case "reddit": catName = "forum"; break;
                        case "espn": catName = "sports"; break;
                        default:
                            throw new IllegalStateException("Unexpected value: " + index);
                    }
                    Main.profile.updateCategories(catName);
                } else {                                    //ELSE TRIES TO IDENTIFY CATEGORY THROUGH KEYWORDS
                    getKeyWords();
                }
            }
        }
    }

    //VARIABLE S IS THE HOST -> EXAMPLE: REDDIT, FACEBOOK, VG
    private boolean searchForHost(String s) {
        Scanner scanner = new Scanner(this.body);       //SCANS THROUGH THE BODY

        //AS LONG AS THERE IS A NEXT LINE
        while (scanner.hasNextLine()) {
            String string = scanner.nextLine();         //SETS STRING AS THE NEXTLINE
            if (string.contains(s)) {                   //IF THE LINE FROM BODY CONTAINS HOST RETURN TRUE
                return true;
            }
        }
        return false;
    }

    private void getKeyWords() {
        int[] intArray = new int[5];

        //GOES THROUGH EVERY LINE OF HTML
        Scanner scanner = new Scanner(this.body);
        //WHILE THERE IS MORE IN BODY
        while (scanner.hasNextLine()) {
            String string = scanner.nextLine().toLowerCase();
            for (String keyword : Main.keyWordsNews) {      //GOES THROUGH THE KEYWORDS OF EACH CATEGORY
                if (string.contains(keyword)) {             //IF A KEYWORD MATCHES ANY OF THE WORDS IN THE LINE
                    ++intArray[0];                          //ADDS UP THE INTARRAY CATEGORY THAT GETS MATCHED
                }
            }
            for (String keyword : Main.keyWordsSports) {
                if (string.contains(keyword)) {
                    ++intArray[1];
                }
            }
            for (String keyword : Main.keyWordsGame) {
                if (string.contains(keyword)) {
                    ++intArray[2];
                }
            }
            for (String keyword : Main.keyWordsForum) {
                if (string.contains(keyword)) {
                    ++intArray[3];
                }
            }
            for (String keyword : Main.keyWordsMedia) {
                if (string.contains(keyword)) {
                    ++intArray[4];
                }
            }
        }

        //FIND LARGEST VALUE == MOST HITS ON KEYWORD CATEGORY
        int largest = 0;
        for (int i = 0; i < intArray.length; i++) {
            //SETS INDEX OF LARGEST AND CHECKS IF ANY LATER ON IS LARGER
            if (intArray[i] > intArray[largest]) largest = i;
        }

        //CHECKS WHICH CATEGORY GOT THE MOST HITS ON KEYWORDS AND SENDS NAME OF CATEGORY TO UPDATECATEGORY
        switch (largest) {
            case 0:
                Main.profile.updateCategories("news");
                break;
            case 1:
                Main.profile.updateCategories("sports");
                break;
            case 2:
                Main.profile.updateCategories("game");
                break;
            case 3:
                Main.profile.updateCategories("forum");
                break;
            case 4:
                Main.profile.updateCategories("media");
                break;

        }

    }

}




/*
if(this.httpObject instanceof FullHttpResponse){
            System.out.println("---------------------------------" +
                    "-------------------FullHttpResponse -----------------------------------" +
                    "-------------------------------");
            FullHttpResponse response = (FullHttpResponse) httpObject;
            CompositeByteBuf contentBuf = (CompositeByteBuf) response.content();
            Controller.contentType = response.headers().get(HttpHeaders.Names.CONTENT_TYPE);
            Controller.status = response.getStatus().toString();

            String contentStr = contentBuf.
                    toString(CharsetUtil.UTF_8);

            /*System.out.println(this.contentType);
            System.out.println(this.host);
            System.out.println(this.encoding);
            System.out.println(httpObject);
            System.out.println(contentStr);


***UNUSED FUNCTION
private Integer eachCategoryFor(String string, List<String> keyWords) { // GETS STRING FROM SCANNER AND KEYWORDCATEGORY
        int categoryNmb = 0;
            for (String keyword : keyWords) {   //ITERATES THROUGH KEYWORDCATEGORY LIST
            if (string.contains(keyword)) {     //IF STRING CONTAINS KEYWORD RETURN CATEGORYNAME

                return categoryNmb; //returns categoryName to other function
            }
        }
        return null; //if cant find keyword in category returns null value
    }
 */

