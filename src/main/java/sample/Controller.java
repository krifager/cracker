package sample;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import org.littleshoot.proxy.HttpFilters;
import org.littleshoot.proxy.HttpFiltersAdapter;
import org.littleshoot.proxy.HttpFiltersSourceAdapter;
import org.littleshoot.proxy.HttpProxyServer;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;
import org.littleshoot.proxy.mitm.CertificateSniffingMitmManager;
import org.littleshoot.proxy.mitm.RootCertificateException;

import java.io.IOException;
import java.util.List;

//RESOURCES
//https://github.com/MediumOne/littleproxy-example/blob/master/src/main/java/m1/learning/littleproxy/example/filters/ReplacePostContentFilterProxy.java
//https://github.com/adamfisk/LittleProxy/issues/273
//https://github.com/ganskef/LittleProxy-mitm/issues/25

public class Controller {

   public static ObservableList<XYChart.Series<String, Double>> list;
    
    public static volatile ObservableList<HttpPacket> packetList;

    //properties for HTTPHISTORY tab
    @FXML
    public TableColumn httpHistoryId;
    @FXML
    public TableColumn httpHistoryHost;
    @FXML
    public TableColumn httpHistoryType;
    @FXML
    public TableColumn httpHistoryContent;
    @FXML
    public TableView<HttpPacket> tableView;

    //properties for PROFILE tab, barchart
    @FXML
    public BarChart barchart;
    @FXML
    public TextArea contentText;
    @FXML
    private NumberAxis y;
    @FXML
    private CategoryAxis x;

    @FXML
    private Button interceptButton;

    @FXML
    private TextArea interceptTextArea;

    @FXML
    private MenuItem closebutton;

    @FXML       //When a response from website is received
    public void initialize() throws RootCertificateException {
        //starts the capturing of http and https packets
        startHttpHistoryCapture();   //Making room for a new row in "HTTP history"
        HttpProxyServer server =
                DefaultHttpProxyServer.bootstrap()
                        .withPort(8080)
                        .withFiltersSource(new HttpFiltersSourceAdapter() {
                            @Override
                            public int getMaximumRequestBufferSizeInBytes() { //TO BUFFER THE REQUESTS THIS NEEDS TO BE SET
                                return 1024 * 1024;
                            }
                            @Override
                            public int getMaximumResponseBufferSizeInBytes() { //TO BUFFER THE RESPONSE THIS NEEDS TO BE SET
                                return 1024 * 1024 * 2;
                            }
                            public HttpFilters filterRequest(HttpRequest originalRequest, ChannelHandlerContext ctx) {
                                return new HttpFiltersAdapter(originalRequest) {
                                    @Override
                                    public HttpResponse clientToProxyRequest(HttpObject httpObject) {
                                        //while(Main.intercept.get()){}//spool until global variable changes
                                        //STORE REQUEST AND BODY IN LIST
                                        if (httpObject instanceof HttpContent) {   //
                                            String headers = httpObject.toString();  //Storing data from response for later use
                                            String body = ((HttpContent) httpObject).copy().content().toString(CharsetUtil.UTF_8);
                                            String host = ((FullHttpRequest) httpObject).headers().get(HttpHeaders.Names.HOST);
                                            String type = ((FullHttpRequest) httpObject).getMethod().toString();

                                            host = getDomainName(host);  //FUNCTION FOR STORING HOST MORE USER FRIENDLY
                                            //IF REQUEST GOES TO AN UNVISITED SITE, AND NOT AD SITE, ADD IT TO CONNECTIONS AND MAKE A NEW SITE INSTANCE
                                            if(type != null && type.equals("CONNECT") && !Main.connects.contains(host)
                                                    && !host.contains("ad")){
                                                Main.connects.add(host);
                                                Site site = new Site();
                                                Main.sites.add(site);
                                            }
                                            //THIS THREAD STORES THE HEAD AND BODIES SENT TO A SITE
                                            Thread httpRequestHandler = new Thread(new HttpRequestHandler(headers,body,host));
                                            httpRequestHandler.start();

                                            String finalHost = host;
                                            Platform.runLater(new Runnable() {  //ADDING REQUESTS TO ObservableList AND ADDING CHANGES IN LIST TO HTTPHISTORY
                                                @Override
                                                public void run() {
                                                    Controller.packetList.add(new HttpPacket(
                                                            (int) Main.packetCounter.getAndIncrement(),
                                                            finalHost,
                                                            type,
                                                            headers + body));
                                                    tableView.setItems(Controller.packetList);
                                                    tableView.getColumns().setAll(httpHistoryId,httpHistoryHost,httpHistoryType,httpHistoryContent);
                                                }
                                            });
                                        }
                                        //WHEN INTERCEPT IS OFF, FORWARD THE REQUEST TO BROWSER
                                        return super.proxyToServerRequest(httpObject);
                                    }
                                    @Override
                                    public HttpObject serverToProxyResponse(HttpObject httpObject) {
                                        //STORE RESPONSE IN LIST
                                        if (httpObject instanceof HttpContent) {
                                            String headers = httpObject.toString();
                                            String body = ((HttpContent) httpObject).copy().content().toString(CharsetUtil.UTF_8);
                                            String encoding = ((FullHttpResponse) httpObject).copy().headers().get(HttpHeaders.Names.CONTENT_ENCODING);
                                            //DON'T PUT THIS A FUNCTION, LAST TIME IT STOPPED THE PROGRAM :) :
                                            //IF DATA IS RECEIVED, DECODE IT:
                                            if(!body.isEmpty() && encoding != null && !encoding.isEmpty()){
                                                try {
                                                    HtmlDecoder htmlDecoder = null;
                                                    htmlDecoder = new HtmlDecoder(((HttpContent) httpObject).copy(), body, encoding);
                                                    body = htmlDecoder.getDecodedBody();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            //ADDING RESPONSE TO HTTP HISTORY AS WELL
                                            Thread httpResponseHandler = new Thread(new HttpResponseHandler(((HttpContent) httpObject).copy(),headers, body, encoding));
                                            httpResponseHandler.start();
                                            String finalBody = body;
                                            Controller.packetList.add(new HttpPacket(
                                                    (int) Main.packetCounter.getAndIncrement(),
                                                    "UNKNOWN",
                                                    "RESPONSE",
                                                    headers + finalBody));
                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //run in another thread for efficiency
                                                    httpHistoryAction();
                                                }
                                            });
                                        }
                                        return httpObject;   //WHEN INTERCEPT IN TURNED OFF, HTTP OBJECT IS RETURNED TO CLIENT
                                    }
                                };
                            }
                        })// FOR BOTH HTTP AND HTTPS
                        .withManInTheMiddle(new CertificateSniffingMitmManager())
                        .start();

        barchart.setData(getChartData());
    }


    @FXML //updates the http history AND listens to mouseclick on a row
    public void httpHistoryAction(){
        tableView.setItems(Controller.packetList);
        tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("Clicked on row " + (tableView.getSelectionModel().getSelectedCells().get(0)).getRow());
                //System.out.println("Clicked on column " + (tableView.getSelectionModel().getSelectedCells().get(0)).getColumn());
                int index = (tableView.getSelectionModel()
                        .getSelectedCells()
                        .get(0)) //BUG, IF YOU TRY FILTER WE GET AND IndexOutOfBoundsException, filter works anyways
                        .getRow();
                HttpPacket httpPacket = Controller.packetList.get(index);
                contentText.clear();
                contentText.appendText(httpPacket.getContent());
            }
        });
        tableView.getColumns().setAll(httpHistoryId,httpHistoryHost,httpHistoryType,httpHistoryContent);
    }
    //WHY THIS DOESN'T WORK IS A MYSTERY TO ME
    private String decodeResponseBody(HttpObject httpObject, String body, String encoding){
        String decodedString = "";
        if(encoding != null && !encoding.isEmpty()){
            try {
                HtmlDecoder htmlDecoder = null;
                htmlDecoder = new HtmlDecoder(httpObject, body, encoding);
                decodedString= htmlDecoder.getDecodedBody();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return decodedString;
    }

    @FXML   //INITIALIZE A NEW ROW TO BIND VALUES TO IN HISTORY
    private void startHttpHistoryCapture(){
        tableView.setEditable(true);
        httpHistoryId.setCellValueFactory(new PropertyValueFactory<HttpPacket,String>("id"));
        httpHistoryHost.setCellValueFactory(new PropertyValueFactory<HttpPacket,String>("host"));
        httpHistoryType.setCellValueFactory(new PropertyValueFactory<HttpPacket,String>("type"));
        httpHistoryContent.setCellValueFactory(new PropertyValueFactory<HttpPacket,String>("content"));
        tableView.setItems(Controller.packetList);
        packetList = FXCollections.observableArrayList();
    }


    @FXML
    private void interceptButtonAction(){
        if (Main.intercept.get() == false) interceptButton.setText("Intercept On");
        else  interceptButton.setText("Intercept Off");

        Main.intercept.set(!Main.intercept.get());
        Main.interceptTextArea.set(!Main.interceptTextArea.get());
        System.out.println("INTERCEPT PRESSED, VALUE IS " + Main.intercept.get());
    }

    //GETTING DOMAIN NAME FROM URL
    private String getDomainName(String s){
        String[] temp;
        temp = s.split("\\.");              //SPLITS THE URL ON DOTS, IN THE URL
        String string = temp[temp.length - 2];    //TAKES THE SECOND TO LAST STRING IN THE ARRAY  "https://www (domainName) com"
        return string;                            // THEN RETURNS IT
    }

    //CLOSE APPLICATION
    @FXML
    private void closeButtonAction(){
        System.exit(0);
    }


    @FXML
    private ObservableList<XYChart.Series<String, Double>> getChartData() {
        list = FXCollections.observableArrayList();                                      //MAKES AND OBSERVABLE ARRAY LIST
        XYChart.Series<String, Double> aSeries = new XYChart.Series<String, Double>();  //MAKES A NEW XYCHART SERIES, WITH PARAMS
                                                                                       //STRING AND DOUBLE
        aSeries.getData().add(new XYChart.Data("News", 0));   //INITIALIZES THE DATA FOR EACH CATEGORY TO 0
        aSeries.getData().add(new XYChart.Data("Sports", 0));
        aSeries.getData().add(new XYChart.Data("Game", 0));
        aSeries.getData().add(new XYChart.Data("Forum", 0));
        aSeries.getData().add(new XYChart.Data("Media", 0));
        list.add(aSeries);                                          //ADDS THE SERIES IN THE OBSERVABLE LIST

        return list;                                                // RETURNS THE LIST OBJECT TO FUNCTION CALL
    }

    @FXML
    public void saveFileAction(ActionEvent actionEvent) {   //ON SAVE FILE MENUITEM CLICK
        FileReadWrite f = new FileReadWrite();              //MAKES A FILEREADWRITE INSTANCE
        for (HttpPacket httpPacket: Controller.packetList) {    //GOES THROUGH PACKETLIST
            f.FileWrite(httpPacket);                        //SENDS EACH HTTPPACKET TO FILEWRITE
        }
    }

    @FXML
    public void openFileAction(ActionEvent actionEvent) throws IOException {    //ON OPEN FILE MENUITEM CLICK
        FileReadWrite f = new FileReadWrite();                                  //MAKES A FILEREADWRITE INSTANCE
        List<HttpPacket> packets;                                               //MAKES A HTTPPACKET LIST
        packets = f.FileRead();                                         //GETS HTTPPACKETS IN A LIST FROM FILEREAD
        Controller.packetList.addAll(packets);                          //ADDS ALL PACKETS IN THE LIST TO PACKETLIST
        tableView.setItems(Controller.packetList);                      //SETS ITEMS TO SHOW IN TABLE
        tableView.getColumns().setAll(httpHistoryId,httpHistoryHost,httpHistoryType,httpHistoryContent); //SHOWS ITEMS
    }
}

//**********************************************************************************************************************
//***************DELETED CODE THAT IS EITHER TO BE USED LATER OR IS THERE JUST TO REMEMBER WHAT HAS BEEN TRIED**********
//**********************************************************************************************************************
/*

try {
    if (encoding != null && !body.isEmpty() && encoding.equals("br")) {
        System.out.println(encoding);
        HtmlDecoder htmlDecoder = new HtmlDecoder(httpObject, body, encoding);
        body = htmlDecoder.getDecodedBody();
    }
} catch (IOException e) {
    e.printStackTrace();
}
 */


//RUNS THE FUNCTION UPDATE EVERY 2 SECONDS
/*Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(2),
                        actionEvent -> {
                            // Call update method for every 2 sec.
                            update();
                        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();

//REMOVE HEADERS SO WE ALWAYS GET THE HTML RESPONSE
if (httpObject instanceof HttpMessage) {
        HttpHeaders headers = ((HttpMessage)httpObject).headers();
        headers.names().forEach(h -> headers.remove(HttpHeaders.Names.IF_MODIFIED_SINCE));
}

//SHOW HTTP REQUEST ON INTERCEPT SCREEN
if(Main.interceptTextArea.getAndSet(false) && Main.intercept.get()){
    //SHOW HTTP REQUEST ON INTERCEPT SCREEN
}

//SPOOL UNTIL
while(Main.intercept.get()){}//spool until global variable changes

interceptTextArea.clear();


//SOMETHING NOT NEEDED ATM
@Override
public HttpResponse proxyToServerRequest(HttpObject httpObject) {
    /*while(Main.intercept.get()){}

    if(httpObject instanceof DefaultHttpContent) {
        Thread httpResponseHandler = new Thread(new HttpResponseHandler(httpObject));
        httpResponseHandler.start();
    }
    //SEE REQUEST BODY
    if(httpObject instanceof FullHttpRequest){
        FullHttpRequest request = (FullHttpRequest) httpObject;
        CompositeByteBuf contentBuf = (CompositeByteBuf) request.content();

        String contentStr = contentBuf.toString(CharsetUtil.UTF_8);
    }

    return null;
}




while(Main.intercept.get()){}

if(httpObject instanceof DefaultHttpContent) {

}
System.out.println("Response is" + httpObject);


if(httpObject instanceof HttpMessage){
    System.out.println("HttpMessage 1\n\n");
}
if(httpObject instanceof HttpResponse){
    System.out.println("HttpResponse 2\n\n");
    System.out.println(httpObject);
    if(httpObject instanceof DefaultHttpResponse){
        System.out.println("DefaultHttpResponse 3\n\n");
    }
}

if(httpObject instanceof HttpContent){
    CompositeByteBuf contentBuf =  (CompositeByteBuf) ((HttpContent) httpObject).content();
    String contentStr = contentBuf.toString(CharsetUtil.UTF_8);
    System.out.println(contentStr);
    if(httpObject instanceof DefaultHttpContent){
        contentBuf = (CompositeByteBuf) ((DefaultHttpContent) httpObject).content();
        System.out.println(contentStr);
    }
    if(httpObject instanceof LastHttpContent ){
        contentBuf = (CompositeByteBuf) ((LastHttpContent) httpObject).content();
        System.out.println(contentStr);
    }
}

*/