package sample;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.rmi.server.LogStream.log;

public class FileReadWrite {

    // NAME OF THE FILES
    private String text1 = "crackerfile1.txt";
    private String text2 = "crackerfile2.txt";
    private String text3 = "crackerfile3.txt";
    private String text4 = "crackerfile4.txt";

    //WRITING DATA FOR EVERY HTTP OBJECT IN THE "PACKETLIST" LIST TO FILES TO BE STORED/SAVED
    public void FileWrite(HttpPacket packet) {

        //MAKING 4 FILES INSTANCES (1:ID, 2:HOST, 3:TYPE, 4:CONTENT)
        File File1 = new File(text1);
        File File2 = new File(text2);
        File File3 = new File(text3);
        File File4 = new File(text4);

        //IF FILES NOT EXISTS, CREATE THEM:
        if (!File1.exists() && !File2.exists() && !File3.exists() && !File4.exists()) {
            try {
                File1.createNewFile();
                File2.createNewFile();
                File3.createNewFile();
                File4.createNewFile();

            } catch (IOException e) {
                log("Excepton Occured: " + e.toString());
            }
        }
        //WRITING 1:ID, 2:HOST, 3:TYPE, 4:CONTENT TO EACH FILE
        try {
            FileWriter writeToFile = null;
            writeToFile = new FileWriter(File1.getAbsoluteFile(), true);  //MAKING A FILEWRITER TO SPECIFIED FILE
            BufferedWriter bufferWriter = new BufferedWriter(writeToFile);      //BUFFERING DATA TO BE SENT TO FILE
            bufferWriter.write(packet.getId()+ "\n");                       //WRITING IT TO THE FILE

            FileWriter writeToFile2 = null;
            writeToFile2 = new FileWriter(File2.getAbsoluteFile(), true);
            BufferedWriter bufferWriter2 = new BufferedWriter(writeToFile2);
            bufferWriter2.write(packet.getHost()+ "\n");

            FileWriter writeToFile3 = null;
            writeToFile3 = new FileWriter(File3.getAbsoluteFile(), true);
            BufferedWriter bufferWriter3 = new BufferedWriter(writeToFile3);
            bufferWriter3.write(packet.getType()+ "\n");

            FileWriter writeToFile4 = null;
            writeToFile4 = new FileWriter(File4.getAbsoluteFile(), true);
            BufferedWriter bufferWriter4 = new BufferedWriter(writeToFile4);
            bufferWriter4.write(packet.getContent()+ "\n");

            bufferWriter.close();
            bufferWriter2.close();    //CLOSE THE WRITER
            bufferWriter3.close();
            bufferWriter4.close();

        } catch (IOException e) {
            log("Error while saving data to file " + e.toString());
        }
    }

    //FUNCTION TO READ CONTENT OF EACH FILE AND ADDING EACH LINE FROM EACH FILE TO A LIST
    public List<HttpPacket> FileRead() throws IOException {

        List<HttpPacket> packets = new ArrayList<>();  // TEMP ARRAY LIST TO BE RETURNED TO CONTROLLER:
        int index1=0, index2=0, index3=0;
        String line;
        int numberOfLines=0;
        File File1 = new File(text1);              //FILES TO READ FROM:
        File File2 = new File(text2);
        File File3 = new File(text3);
        File File4 = new File(text4);
        List<Integer> id = new ArrayList<>();     //1:ID, 2:HOST, 3:TYPE, 4:CONTENT LISTS:
        List<String> host = new ArrayList<>();
        List<String> type = new ArrayList<>();
        List<String> content = new ArrayList<>();

        //IF FILE EXISTS READ EACH LINE FROM FILE
        if (File1.exists()) {
           BufferedReader reader1 = Files.newBufferedReader(Paths.get(text1));
           while ((line = reader1.readLine()) != null)  {
               id.add(Integer.parseInt(line));   //ADDING THE ID FROM EACH LINE IN FILE TO THE ID ARRAY LIST
           }
        }

        if (File2.exists()){
         BufferedReader reader2 = Files.newBufferedReader(Paths.get(text2));
            while ((line = reader2.readLine()) != null)  {
                host.add(line);
            }
        }

         if (File3.exists()){
        BufferedReader reader3 = Files.newBufferedReader(Paths.get(text3));
             while ((line = reader3.readLine()) != null)  {
                 type.add(line);
             }
        }


          if (File4.exists()){
        BufferedReader reader4 = Files.newBufferedReader(Paths.get(text4));
              while ((line = reader4.readLine()) != null)  {
                   content.add(line);
              }
         }

          //ADDING 1:ID, 2:HOST, 3:TYPE, 4:CONTENT TO PACKETLIST:
        for (int idTemp: id) {
            packets.add(new HttpPacket(idTemp, host.get(index1), type.get(index2), content.get(index3)));
            ++index1;
            ++index2;
            ++index3;
        }
        return packets;
    }
}


/************************************************************************
 * WORKING WRITETOFILE, BUT REPLACED FOR WRITING TO MULTIPLE FILES
try {
        FileWriter writeToFile = null;
        writeToFile = new FileWriter(File1.getAbsoluteFile(), true);
        BufferedWriter bufferWriter = new BufferedWriter(writeToFile);
        bufferWriter.write(packet.getId() + "\n");
        bufferWriter.write(packet.getHost() + "\n");
        bufferWriter.write(packet.getType() + "\n");
        bufferWriter.write(packet.getContent() + "\n");

        bufferWriter.close();

        } catch (IOException e) {
        log("Error while saving data to file " + e.toString());
        }

 * READER FROM FILE, BUT DOESN'T WORK, SUCCESSFULLY READS 1 LINE FOR EACH VALUE, STRUGGLES WITH CONTENT
 * AS ITS OVER MULTIPLE LINES. DUE TO TIME NOT CONTINUED
 List<HttpPacket> packet = new ArrayList<>();
 int id;
 String host;
 String type;
 String content;

 int numberOfLines=0;
 File file = new File(file_location);
 if (file.exists()) {
 Scanner scanner = new Scanner(file);
 while (scanner.hasNextLine()) {
 id = Integer.parseInt(scanner.nextLine());
 host = scanner.nextLine();
 type = scanner.nextLine();
 content = scanner.nextLine();

 packet.add(new HttpPacket(id,host,type,content));
*/