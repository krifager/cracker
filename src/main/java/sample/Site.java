package sample;

import java.util.ArrayList;
import java.util.List;

/***********************************************************************************************************************
 *********************CREATES A SITE FOR EACH RESPONSE+REQUEST TO STORE THE DATA OF THE SITES VISITED*******************
 **********************************************************************************************************************/

public class Site {

    private static volatile List<String> responses;
    private static volatile List<String> responsesBody;
    private static volatile List<String> requests;
    private static volatile List<String> requestsBody;

    public Site(){
        this.responses = new ArrayList<>();     //INITIALIZES THE ARRAYLIST
        this.responsesBody = new ArrayList<>(); //INITIALIZES THE ARRAYLIST
        this.requests = new ArrayList<>();      //INITIALIZES THE ARRAYLIST
        this.requestsBody = new ArrayList<>();  //INITIALIZES THE ARRAYLIST
    }

    public void addRequest(String header, String body){
        this.requests.add(header);              //ADDS HEADER VALUE TO THE REQUEST
        this.requestsBody.add(body);            //ADDS BODY VALUES TO THE REQUESTBODY
    }
    public void addResponse(String header, String body){
        this.responses.add(header);             //ADDS HEADER VALUE TO THE RESPONSES
        this.responsesBody.add(body);           //ADDS BODY VALUE TO THE RESPONSESBODY
    }
}
