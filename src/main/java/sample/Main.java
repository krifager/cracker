package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class Main extends Application {

    //ATOMIC VALUES FOR THREAD SYNC
    public static AtomicBoolean intercept;
    public static AtomicBoolean interceptTextArea;
    //LIST OF PROFILES AND CONNECTS
    public static volatile List<Site> sites;//            siteReddit        siteBlah           siteFacebook
    public static volatile List<String> connects;//          reddit           blah               facebook
    //PROFILER INSTANCE
    public static volatile Profiler profile;
    //KEYWORDS FOR CATEGORY CHECKS
    public static volatile List<String> keyWordsNews;
    public static volatile List<String> keyWordsSports;
    public static volatile List<String> keyWordsGame;
    public static volatile List<String> keyWordsForum;
    public static volatile List<String> keyWordsMedia;
    //COUNTS THE NUMBER OF REQUEST AND RESPONSES
    public static AtomicLong packetCounter;
    //POPULARSITES STRING LIST
    public static volatile List<String> popularSites;

    @Override
    public void start(Stage primaryStage) throws Exception{
        URL url = new File("src/main/java/sample/sample.fxml").toURI().toURL();
        Parent root = FXMLLoader.load(url);
        primaryStage.setTitle("Cracker");
        primaryStage.setScene(new Scene(root, 900, 750));
        primaryStage.show();
    }


    public static void main(String[] args) {
        //SETS INTERCEPT AND INTERCEPTTEXTAREA ATOMICBOOLEAN TO TRUE
        intercept = new AtomicBoolean(true);
        interceptTextArea = new AtomicBoolean(true);

        //LIST USED GLOBALLY
        sites = new ArrayList<>();
        connects = new ArrayList<>();

        //INITIALIZATION OF KEYWORDS LIST
        keyWordsNews = new ArrayList<>();
        keyWordsGame = new ArrayList<>();
        keyWordsSports = new ArrayList<>();
        keyWordsForum = new ArrayList<>();
        keyWordsMedia = new ArrayList<>();

        //KEYWORDS
        keyWordsNews.add("news");keyWordsNews.add("nyhet");keyWordsNews.add("nytt");keyWordsNews.add("avis");keyWordsNews.add("newspaper");
        keyWordsGame.add("spill");keyWordsGame.add("game");keyWordsGame.add("coding");keyWordsGame.add("nettspill");
        keyWordsForum.add("økonomi");keyWordsForum.add("reise");keyWordsForum.add("jobb");keyWordsForum.add("søk");
        keyWordsMedia.add("share");keyWordsMedia.add("comments");keyWordsMedia.add("social");keyWordsMedia.add("friends");
        keyWordsSports.add("sports");keyWordsSports.add("team");keyWordsSports.add("football");

        //INITIALIZATION OF POPULARSITES LIST
        popularSites = new ArrayList<>();
        popularSites.add("facebook");popularSites.add("twitter");popularSites.add("youtube");popularSites.add("instagram");
        popularSites.add("vg");popularSites.add("blackboard");popularSites.add("twitch");popularSites.add("stackoverflow");
        popularSites.add("reddit");popularSites.add("espn");popularSites.add("mafiaspillet");

        //START PROFILER
        profile = new Profiler();

        //PACKET COUNTER
        packetCounter = new AtomicLong(0);

        launch(args);
    }
}
