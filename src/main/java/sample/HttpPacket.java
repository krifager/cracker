package sample;

import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpObject;
import io.netty.util.CharsetUtil;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

import java.util.concurrent.atomic.AtomicLong;

public class HttpPacket {
    private SimpleIntegerProperty id;
    private SimpleStringProperty host;
    private SimpleStringProperty type;
    private SimpleStringProperty content;

    //CALLS ON FUNCTIONS TO SET AND ADD THE VALUES ID, HOST, TYPE AND CONTENT TO PACKETLIST
    public HttpPacket(int id, String host, String type, String content){
        //initialize
        this.id = new SimpleIntegerProperty();
        this.host = new SimpleStringProperty();
        this.type = new SimpleStringProperty();
        this.content = new SimpleStringProperty();
        //set values
        this.id.set(id);
        this.host.set(host);
        this.type.set(type);
        this.content.set(content);
    }

   //SETS THE VALUES HERE
    public SimpleIntegerProperty getIdProperty(){
        return this.id;
    }
    public final int getId(){
        return this.id.get();
    }
    public final void setId(int id){this.id.set(id);}

    public SimpleStringProperty getHostProperty(){
        return this.host;
    }
    public final String getHost() { return this.host.get(); }
    public final void setHost(String name) { this.host.set(name); }

    public SimpleStringProperty getTypeProperty(){
        return this.type;
    }
    public final String getType() { return this.type.get(); }
    public final void setType(String name) { this.type.set(name); }

    public SimpleStringProperty getContentProperty(){
        return this.content;
    }
    public final String getContent() { return this.content.get(); }
    public final void setContent(String name) { this.content.set(name); }
}
