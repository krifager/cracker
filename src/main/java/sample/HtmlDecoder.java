package sample;


import io.netty.buffer.ByteBufInputStream;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpObject;
import org.brotli.dec.BrotliInputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/***********************************************************************************************************************
**************************************THIS CLASS DECODES THE GIVEN ENCODED HTML*****************************************
***********************************************************************************************************************/

public class HtmlDecoder{

    private HttpObject httpObject;
    private String html;
    private String encoding;
    //private int BYTE_SIZE = 10000;
                                    //CONSTRUCTOR SETS THE PRIVATE VALUES OF THE CLASS
    public HtmlDecoder(HttpObject httpObject, String body, String encoding) throws IOException {
        this.httpObject = httpObject;
        this.html = body;
        this.encoding = encoding;
        //AT THIS MOMENT THE DECODER ONLY DECODES BR
        if(this.encoding.equals("br")){             //IF THE ENCODING IS BR(BROTLI) ENCODED, SEND TO DECODER
            BrotliDecoder brotliDecoder = new BrotliDecoder();
            this.html = brotliDecoder.decode(this.httpObject, this.html);
        }
    }


    public String getDecodedBody(){
        return this.html;
    }

    //DECODER FOR BROTLI CONTENT_ENCODING
    private static class BrotliDecoder {
                        //DECODER IMPORTS THE CURRENT HTTPOBJECT OF HTTPCONTENT AND RETURNS IT AS A DECODED STRING
        public String decode(HttpObject httpObject, String html) throws IOException {
            String decodedString = "";
            BufferedReader rd = new BufferedReader( //CREATES A BUFFERED READER WITH AN INPUTSTREAM READER FOR
                    new InputStreamReader(          //BROTLIINPUTSTREAM RETURN VALUE AND CREATES A BYTEBUFINPUTSTREAM
                            new BrotliInputStream(  //OF THE FULLHTTPRESPONSE CONTENT TO SEND INTO THE BROTLI READER
                                    new ByteBufInputStream(((FullHttpResponse) httpObject).content()))));

            StringBuilder result = new StringBuilder(); //CREATES A NEW STRINGBUILDER
            String line = "";
            while ((line = rd.readLine()) != null) {    //READS THE BUFFEREDREADERS CONTENT UNTIL NO MORE LINES
                result.append(line);                    //READ LINES IS THEN APPENDED TO THE RESULT STRINGBUILDER
            }
            //System.out.println(result);
            decodedString = result.toString();          //THE RESULT IS THEN STORED IN THE DECODEDSTRING AS A STRING
            return decodedString;                       //AND RETURNS IT TO FUNCTION CALL
        }
    }
}
